-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Linux (x86_64)
--
-- Host: nt71li6axbkq1q6a.cbetxkdyhwsb.us-east-1.rds.amazonaws.com    Database: bzhxnkn1g18z3d6n
-- ------------------------------------------------------
-- Server version	10.1.19-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app`
--

DROP TABLE IF EXISTS `app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app`
--

LOCK TABLES `app` WRITE;
/*!40000 ALTER TABLE `app` DISABLE KEYS */;
INSERT INTO `app` VALUES (1,'1812.10');
/*!40000 ALTER TABLE `app` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `icon`
--

DROP TABLE IF EXISTS `icon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `icon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `icon`
--

LOCK TABLES `icon` WRITE;
/*!40000 ALTER TABLE `icon` DISABLE KEYS */;
/*!40000 ALTER TABLE `icon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poem`
--

DROP TABLE IF EXISTS `poem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idPresent` int(11) DEFAULT NULL,
  `title` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `idPresent` (`idPresent`),
  CONSTRAINT `poem_ibfk_1` FOREIGN KEY (`idPresent`) REFERENCES `present` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poem`
--

LOCK TABLES `poem` WRITE;
/*!40000 ALTER TABLE `poem` DISABLE KEYS */;
INSERT INTO `poem` VALUES (7,4,'Poema Express','Amor de mi vida\ngatita de mi corazón\nestas palabras te escribo\ncon locura e inspiración.\n\nPoema express es este\nque te escribo con deleite\neres de mi vida mi amor\nmi bella y hermosa canción.\n\nDe mis sueños eres mi miau\nde mis dias mi verdadero amor\nquien me ama con pasión\ny aunque me viole no me deja dolor.\n\nEres la luz de mis días\nmi mariposa de mis jardines\nla abejita de mis flores\nla arañita tigre de mis rincones.\n\nTienes un apetito voraz\ncuando de pasion ha de tratar\nel que busco a diario saciar\npero no hayo como calmar.\n\nEres una hermosa perlita\npuesta por ahi en una conchita\nque embellece el gran mar\ny a diario osa de deleitar.\n\nAmor de mi vida\ngatita de mi corazon\njamas me cansare de escribir\nsobre ti y nuestro amor.'),(8,5,'Miau Sorprecita','Amor de mi vida\ntu haces mi felicidad\nen cada momento que pasa\ndemostrandolo en cada beso\n\nEres el amor de mi vida\nenamorándome cada día\ncon tu desplante y belleza\npero bueno, ya no es una sorpresa\n\nTe amo a cada instante\nbien loco me tienes\npero sin duda alguna\nte amo porque siempre me prefieres\n\nSoy un chico consentido\nasí me has acostumbrado\npero más que el amor\nte amo por haberme valorado'),(10,7,'Primer Día','Primer dia para miau\nen su nueva labor\nsacrificandose dia a dia\npor un futuro mejor\n\nSiempre has sido luchadora\nesta no es la excepción\nsé que llegarás lejos\ny si te lo propones\nseras la mejor\n\nTe he visto luchando\ncada día por tus metas\nderrotando al cansancio,\nla adversidad y la pereza\n\nEres un ángel para todos\nuna luz en mi vida\nun ejemplo a seguir\nsobretodo digno de admirar\n\nEres una mujer deslumbrante\nlejos un brillante as\na quien no me canso\nde dia a día amar\n\nSigue siempre luchando\npor tu futuro y sueños\nconfío en que podrás\nporque tú eres capaz\n\nTe amo con orgullo\ndeseo y felicidad\neres mi gran anhelo\nen mis ratos de soledad\n\nTú eres todo\nlucha, inteligencia y superación\nno olvides jamás\nque tu escencia es la humildad\n\nLucha siempre lucha\nasí los sueños se consiguen\nyo se que lo lograrás\ny cuando seas anciana\nde acordarte solo te reirás'),(11,8,'Dos añitos de amor','Dos añitos de amor\ndos añitos de pasión\ndos añitos de lucha\ndos añitos de superación\n\nAmor mío cada día más te amo\ncada día me vuelves loco\nsiento que no puedo más\ncon este delirio en mi ser\n\nMe haces muy feliz\na cada dia y a cada instante\nme enloqueces a diario\ny a cada minuto te amo\n\nGracias por llegar a mi vida\npor cada instante a tu lado\npero sé que es el comienzo\nde una vida a tu lado\ncautivo y enamorado'),(12,9,'Difícil','Difícil es no besarte\ndifícil es no abrazarte\ndifícil es no amarte\ndifícil es lejos de tí quedarme\n\nDifícil es estar sin ti\ndifícil es dormir a solas\ndifícil es un despertar incompleto\ndifícil es tener un solitario desvelo\n\nA ti amor mío decirte quiero\nque mis sentimientos por ti\nson perversos pero sinceros\n\nA ti mi vida\nsin ti no puedo\nvivir un segundo porque\nnecesito de ti, mi cielo');
/*!40000 ALTER TABLE `poem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `present`
--

DROP TABLE IF EXISTS `present`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `present` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idType` int(11) DEFAULT NULL,
  `icon` blob,
  `datetime` datetime DEFAULT NULL,
  `readed` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idType` (`idType`),
  CONSTRAINT `present_ibfk_1` FOREIGN KEY (`idType`) REFERENCES `presentType` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `present`
--

LOCK TABLES `present` WRITE;
/*!40000 ALTER TABLE `present` DISABLE KEYS */;
INSERT INTO `present` VALUES (4,1,NULL,'2018-12-13 20:22:58',''),(5,1,NULL,'2018-12-15 22:02:19',''),(7,1,NULL,'2018-12-18 22:02:17',''),(8,1,NULL,'2019-01-06 22:09:03',''),(9,1,NULL,'2019-01-15 18:08:00','');
/*!40000 ALTER TABLE `present` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `presentType`
--

DROP TABLE IF EXISTS `presentType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `presentType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `presentType`
--

LOCK TABLES `presentType` WRITE;
/*!40000 ALTER TABLE `presentType` DISABLE KEYS */;
INSERT INTO `presentType` VALUES (1,'Normal');
/*!40000 ALTER TABLE `presentType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` int(11) NOT NULL,
  `nick` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passw` blob,
  PRIMARY KEY (`id`),
  KEY `icon` (`icon`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`icon`) REFERENCES `icon` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `viewPresentMenu`
--

DROP TABLE IF EXISTS `viewPresentMenu`;
/*!50001 DROP VIEW IF EXISTS `viewPresentMenu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `viewPresentMenu` (
  `id` tinyint NOT NULL,
  `icon` tinyint NOT NULL,
  `title` tinyint NOT NULL,
  `datetime` tinyint NOT NULL,
  `readed` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `viewPresentMenu`
--

/*!50001 DROP TABLE IF EXISTS `viewPresentMenu`*/;
/*!50001 DROP VIEW IF EXISTS `viewPresentMenu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`n23cres05fqbhzfd`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `viewPresentMenu` AS select `p`.`id` AS `id`,`p`.`icon` AS `icon`,`po`.`title` AS `title`,`p`.`datetime` AS `datetime`,`p`.`readed` AS `readed` from (`present` `p` join `poem` `po`) where (`p`.`id` = `po`.`idPresent`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-01  8:21:36
